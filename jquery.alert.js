(function ($) {
	$.defaults = {};
	$.defaults.info = {
		'message': '',
		'title': '',
		'type': 'info',
		'icon': '',
		'open': {
			'delay': 0,
			'duration': 500,
			'callback': function () {}
		},
		'close': {
			'delay': 5000,
			'duration': 500,
			'callback': function () {}
		},
		'element': {
			'container': 'div',
			'box': 'div',
			'icon': 'span',
			'title': 'h3',
			'message': 'p'
		},
		'target': 'body',
		'class': {
			'container': ['jquery-alert-container'],
			'box': ['jquery-alert-box'],
			'icon': ['jquery-alert-icon'],
			'title': ['jquery-alert-title'],
			'message': ['jquery-alert-message']
		},
		'style': {
			'container': {},
			'box': {},
			'icon': {},
			'title': {},
			'message': {}
		}
	};
	$.defaults.error = $.extend(true, {}, $.defaults.info, {'type': 'error'});
	$.defaults.success = $.extend(true, {}, $.defaults.info, {'type': 'success'});

	$.overrideDefaults = function (options) {
		if ((typeof options).toUpperCase() == 'object'.toUpperCase()) {
			var types = [];
			if (arguments.length > 1) for (var index = 1; index < arguments.length; index++) if ('success' == arguments[index] || 'info' == arguments[index] || 'error' == arguments[index]) types.push(arguments[index]);

			if (types.length > 0) {
				for (var index = 1; index < types.length; index++) $.extend(true, $.defaults[types[index]], options);
			} else {
				$.extend(true, $.defaults.info, options);
				$.extend(true, $.defaults.error, options);
				$.extend(true, $.defaults.success, options);
			}
		}
	};

	$.info = function (options) {
		var defaults = $.defaults.info;
		if ((typeof options).toUpperCase() != 'object'.toUpperCase()) {
			var opt = {};
			opt.message = options;
			options = opt;
		}
		options = $.extend(true, {}, defaults, options);

		$.each(options['class'], function (key, value) {
			if ($.inArray(options.type, value) < 0) value.push(options.type);
		});
		var oldBox = options.element.box + '.' + options['class'].box.join('.');
		$(oldBox).remove();
		function open() {
			var open = options.open;
			var $this = $(this);
			$this.delay(open.delay).fadeIn(open.duration, function () {
				$this.removeClass('hidden');
				open.callback.call($this);
			});
		}
		function close() {
			var close = options.close;
			var $this = $(this);
			$this.delay(close.delay).fadeOut(close.duration, function () {
				close.callback.call($this);
				$this.remove();
			});
		}
		var container = $('<' + options.element.container + '>').css(options.style.container).addClass('hidden');
		$(options['class'].container).each(function () {
			var classes = $(container).attr('class');
			$(container).attr('class', (classes ? classes + ' ' : '') + this);
		});
		var box = $('<' + options.element.box + '>').css(options.style.box);
		$(options['class'].box).each(function () {
			var classes = $(box).attr('class');
			$(box).attr('class', (classes ? classes + ' ' : '') + this);
		});
		var title = null;
		if (options.title.length > 0) title = $('<' + options.element.title + '>').css(options.style.title).append(options.title);
		if (title != null) $(options['class'].title).each(function () {
			var classes = $(title).attr('class');
			$(title).attr('class', (classes ? classes + ' ' : '') + this);
		});
		var icon = $('<' + options.element.icon + '>').css(options.style.icon).text(options.type);
		if (options.icon && options.icon.length > 0) icon.css('background-image', 'url(' + options.icon + ')');
		$(options['class'].icon).each(function () {
			var classes = $(icon).attr('class');
			$(icon).attr('class', (classes ? classes + ' ' : '') + this);
		});
		var message = $('<' + options.element.message + '>').css(options.style.message).append(options.message);
		$(options['class'].message).each(function () {
			var classes = $(message).attr('class');
			$(message).attr('class', (classes ? classes + ' ' : '') + this);
		});
		if (title != null) title.appendTo(box);
		icon.appendTo(box);
		message.appendTo(box);
		$('<div>').css('clear', 'both').appendTo(box);
		box.appendTo(container).on('click', function () {
			$this = $(this).parent();
			$this.clearQueue().fadeOut(options.close.duration, function () {
				options.close.callback.call($this);
				$this.remove();
			});
		});
		container.appendTo(options.target);
		open.call(container);
		close.call(container);
	};
	$.error = function (options) {
		var defaults = $.defaults.error;
		if ((typeof options).toUpperCase() != 'object'.toUpperCase()) {
			var opt = {};
			opt.message = options;
			options = opt;
		}
		options = $.extend(true, {}, defaults, options);
		$.info(options);
	};
	$.success = function (options) {
		var defaults = $.defaults.success;
		if ((typeof options).toUpperCase() != 'object'.toUpperCase()) {
			var opt = {};
			opt.message = options;
			options = opt;
		}
		options = $.extend(true, {}, defaults, options);
		$.info(options);
	};
})(jQuery);